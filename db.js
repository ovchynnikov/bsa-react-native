export default contacts = [{
    id: '1',
    firstName: 'John',
    lastName: 'Doe',
    email: 'johndoe@gmail.com',
    phone: '+38 063 555 11 11'

}, {
    id: '2',
    firstName: 'Adam',
    lastName: 'Doe',
    email: 'adamdoe@gmail.com',
    phone: '+38 063 444 11 11'

}, {
    id: '3',
    firstName: 'Alex',
    lastName: 'Smith',
    email: 'alexsmith@gmail.com',
    phone: '+38 063 111 11 11'
}, {
    id: '4',
    firstName: 'James',
    lastName: 'Doe',
    email: 'jamesdoe@gmail.com',
    phone: '+38 063 222 11 11'

}, {
    id: '5',
    firstName: 'Bruse',
    lastName: 'Doe',
    email: 'brusedoe@gmail.com',
    phone: '+38 063 333 11 11'
}, {
    id: '6',
    firstName: 'Bruse',
    lastName: 'Doe',
    email: 'brusedoe@gmail.com',
    phone: '+38 063 333 11 11'
}, {
    id: '7',
    firstName: 'Mark',
    lastName: 'Doe',
    email: 'Markdoe@gmail.com',
    phone: '+38 063 333 11 11'
}, {
    id: '8',
    firstName: 'Angela',
    lastName: 'Doe',
    email: 'Angeladoe@gmail.com',
    phone: '+38 063 333 11 11'
}, {
    id: '9',
    firstName: 'Tomas',
    lastName: 'Doe',
    email: 'Tomasdoe@gmail.com',
    phone: '+38 063 333 11 11'
}, {
    id: '10',
    firstName: 'Bob',
    lastName: 'Doe',
    email: 'Bobdoe@gmail.com',
    phone: '+38 063 333 11 11'
}, {
    id: '11',
    firstName: 'Dima',
    lastName: 'Doe',
    email: 'Dimadoe@gmail.com',
    phone: '+38 063 333 11 11'
}, {
    id: '12',
    firstName: 'Marie',
    lastName: 'Doe',
    email: 'Mariedoe@gmail.com',
    phone: '+38 063 333 11 11'
}, {
    id: '13',
    firstName: 'Vasiliy',
    lastName: 'Doe',
    email: 'Vasiliydoe@gmail.com',
    phone: '+38 063 333 11 11'
}, ]