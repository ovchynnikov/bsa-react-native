import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';

import React, { useEffect, useState } from 'react';
import { StyleSheet, PermissionsAndroid } from 'react-native';
import ContactListComponent from './Components/ContactList';
// import contacts from './db.js';
import { createStackNavigator } from '@react-navigation/stack';
import ContactDetails from './Components/ContactDetails';
import * as Contacts from 'expo-contacts';

const Stack = createStackNavigator();


export default function App() {
	let [contactsList, setContacts] = useState([]);
	let [search, setSearch] = useState();


	useEffect(() => {
    (async () => {
      const { status } = await Contacts.requestPermissionsAsync();
      if (status === 'granted') {
        const { data } = await Contacts.getContactsAsync({
          emailFields: [Contacts.Fields.Emails],
          phoneFields: [Contacts.Fields.PhoneNumbers],
        });

        if (data.length > 0) {
          const contact = data[0];
					setContacts(data);
          // console.log('contact expo',data);
        }
      }
    })();
  }, []);

	useEffect(() => {
		(async () => {
		try {
			const granted = await PermissionsAndroid.request(
				PermissionsAndroid.PERMISSIONS.WRITE_CONTACTS,
				{
					title: "Write contact Permission",
					message:
						"Contact App needs access to your contacts " +
						"so you can add new contacts.",
					buttonNeutral: "Ask Me Later",
					buttonNegative: "Cancel",
					buttonPositive: "OK"
				}
			);
			if (granted === PermissionsAndroid.RESULTS.GRANTED) {
				console.log("You can write contacts");
			} else {
				console.log("WRITE CONTACTS permission denied");
			}
		} catch (err) {
			console.warn(err);
		}
	})()
	}, [])


	updateSearch = (search) => {
    setSearch(search);
  };

  return (
		<NavigationContainer style={styles.container}>
			<Stack.Navigator>
        <Stack.Screen name="Contacts" >
					{props => <ContactListComponent 
					    {...props} 
					    contactsList={contactsList} 
					    updateSearch={updateSearch}
					    search={search}
					/>}
				</Stack.Screen>
				<Stack.Screen name="ContactDetails" component={ContactDetails} />

      </Stack.Navigator>

		</NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
	search: {
		marginTop: 0,
		width: '90%',
		marginBottom: 10,
	}
});
