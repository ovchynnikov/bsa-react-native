import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Icon } from 'react-native-elements';

const Contact = (props) => {
	// console.log('--------props.contact.id-------',props.contact.item.id);
	// console.log('--------Contact props-------',props);
	const {id, firstName, lastName} = props.contact.item;
	const {email} = props.contact.item.emails[0];
  return (
		<TouchableOpacity onPress={() => props.onPress(props.contact.item.id)}>
    <View style={styles.border, styles.container} key={ id }>
			<Icon name='address-card-o' type='font-awesome' />
			<View>
        <Text style={styles.name}>{firstName} {lastName}</Text>
        <Text>{email}</Text>
			</View>
			<Icon name='info-circle'
			      type='font-awesome' 
						/>
    </View>
		</TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
		width: '90%',
		marginBottom: 15,
		marginLeft: 20,
		padding: 10,
    backgroundColor: 'lightgrey',
    alignItems: 'center',
    justifyContent: 'space-between',
		flexDirection: 'row'
  },
	name: {
		marginLeft: 0,
		fontWeight: 'bold',
		
	},
});

export default Contact;