import React, { useState } from 'react';
import { StyleSheet, View, SafeAreaView, FlatList } from 'react-native';
import { Text } from 'react-native-elements';
import { Button } from 'react-native-elements';
import SearchBar from 'react-native-elements/dist/searchbar/SearchBar-ios';
import Contact from './Contact.js';

const ContactListComponent = (props) => {
	const [searchValue, setSearchValue] = useState('');
	// const [searchResult, setSearchResult] = useState([]);

	const onButtonPress = (selectedId) => {
		const selectedContact = props.contactsList.filter((item) => item.id === selectedId);
		props.navigation.navigate('ContactDetails', { contact: selectedContact[0] });
	}

	const filteredContactsList = async () => {

		console.log('props.contactsLis, searchValue');
		console.log(props.contactsLis, searchValue);

		const result = await props.contactsList.filter(
			(item) => searchValue.toUpperCase().trim() === item.firstName.toUpperCase() 
			       || searchValue.toUpperCase().trim() === item.lastName.toUpperCase()
			       || searchValue.toUpperCase().trim() === item.firstName.toUpperCase() + ' ' + item.lastName.toUpperCase()
		)
		// setSearchResult(result);
			return result;
	}
		
	updateSearch = (search) => {
		// console.log(search);
    setSearchValue(search)
  };

	const renderItem = ( props ) => {
		
    return (
       <Contact
			 contact={props}
			 onPress={(id) => onButtonPress(id)}
       />
    );
  };

	if(props.contactsList.length > 0) {
		return (<>
		<SearchBar
        placeholder="Type Here..."
        onChangeText={updateSearch}
        value={searchValue}
    />
		<SafeAreaView style={styles.container}>
			{searchValue.length > 0 ? 
			<FlatList
			  data={filteredContactsList()}
			  renderItem={renderItem}
			  keyExtractor={(item) => item.id}
			  extraData={props.contactsList}
			/> : 
			<FlatList
	      data={props.contactsList}
	      renderItem={renderItem}
	      keyExtractor={(item) => item.id}
	      extraData={props.contactsList}
	    />}
    </SafeAreaView>
			<View style={styles.button}>
			<Button title="Add +"/>
			</View>

			</>)
			} else return <Text style={styles.noData}>No contacts</Text>
}

const styles = StyleSheet.create({
  noData: {
		width: '100%',
		marginLeft: 10,
		fontSize: 20,
		alignItems: 'center',
    justifyContent: 'center',
	},
	button: {
		width: '20%',
		marginTop: 20,
		marginRight: 15,
		marginBottom: 15,
		justifyContent: 'center',
		position: 'absolute',
		bottom: 0,
		right: 0
		

	}
})

export default ContactListComponent;