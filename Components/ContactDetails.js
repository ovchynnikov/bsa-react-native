import React, { useEffect } from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import { Button } from 'react-native-elements';
import * as Contacts from 'expo-contacts';

const ContactDetails = ({ route }) => {
	console.log('route', route.params.contact);
	const { email } = route.params.contact.emails[0];
	const { number } = route.params.contact.phoneNumbers[0];
	const { firstName, lastName, id } = route.params.contact;
	const uri = route.params.contact.image.uri;

	const removeContact = async (id) => {
		try {
			const result = await Contacts.removeContactAsync(id);
		console.log('result',result);
		} catch (err) {
			console.warn(err);
		}
	}

	useEffect(() => {
    (async () => {
      const { status } = await Contacts.requestPermissionsAsync();
      if (status === 'granted') {
        console.log('permission granted');
      }
    })();
  }, []);



  return (
    <View style={styles.container}>
			<Image source={{uri}} style={styles.image}/>
			<View style={styles.userInfo}>
				<Text>Name</Text>
				<Text style={[styles.nameSize, styles.underline]}>{firstName} {lastName}</Text>
				<Text>Phone</Text>
      	<Text style={[styles.numberSize, styles.underline]}>{number}</Text>
				<Text>Email</Text>
      	<Text style={[styles.emailSize, styles.underline]}>{email}</Text>
			</View>
			<View style={styles.buttonBlock}>
				<View style={styles.button}>
					<Button title="Call"  
					        icon={{
                  name: "phone",
                  size: 25,
                  color: "white"}}>	
					</Button>
				</View>
				<View style={styles.button}>
					<Button title="Delete"
									onPress={()=> removeContact(id)}
					        icon={{
					        name: "delete",
					        size: 25,
					        color: "white"}}>
					</Button>
				</View>
			</View>
    </View>
  );
}
	
const styles = StyleSheet.create({
  container: {
		width: '90%',
		marginTop: 15,
		marginLeft: 20,
		padding: 10,
    alignItems: 'center',
    justifyContent: 'flex-start',
		top: 0
  },
	image: {
		position: 'absolute',
		top: 10,
		width: 250,
		height: 250
	  },
	buttonBlock: {
		width: '90%',
		marginTop: 45,
		flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
	},
	button: {
		width: '40%'
	},
	userInfo: {
		marginTop: 270,
	},
	underline: {
		borderBottomWidth:  3,
		borderColor: 'silver',
		marginBottom: 15
	},
	nameSize: {
		fontSize: 24
	},
	emailSize: {
		fontSize: 20
	},
	numberSize: {
		fontSize: 24
	}
});

export default ContactDetails;